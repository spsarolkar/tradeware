module PortUtils
where

import System.Environment
import System.Process (spawnProcess,readProcess,readProcessWithExitCode)
import Data.ByteString.Char8 (pack, unpack)
import Data.List(isInfixOf)
import System.Random (randomRIO)
import Control.Monad.IO.Class (liftIO)

getFreePort = do
		port <- liftIO $ randomRIO (2000::Int, 65535)
		flag <- checkIfFree port
		if flag then do return port else getFreePort

checkIfFree port = do
		putStrLn $ "checking..."++(show port)
		let cmd="lsof"
		--out <- readProcess cmd ["-i","-n","-P",":"++port] []
		--out <- readProcess cmd ["-t","-i",":"++(show port)] []
		(_,out,err) <- readProcessWithExitCode cmd ["-t","-i",":"++(show port)] []
		putStrLn $ "out:"++out
		putStrLn $ "err:"++err
		return $ (length $ lines out) ==0
		--return $ (length $ filter (isInfixOf (show port)) $ lines out) == 0
