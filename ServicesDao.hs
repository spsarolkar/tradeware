module ServicesDao
where

import Database.SednaTypes
import Database.SednaBindings
import Prelude hiding (catch)
import Control.Exception
import Database.SednaBindings
import Database.Internal.SednaConnectionAttributes
import Text.XML.HXT.Core
import Data.Text.Internal
import qualified Data.Text as T
import Data.Text
import Database.SednaExceptions
import Database.Internal.SednaConnectionAttributes

dbconnect = do
        let url="localhost"
        let dbname="services"
        let login="SYSTEM"
        let password="MANAGER"
        sednaConnect url dbname login password

updateService name port typ= do
			sednaCon <- dbconnect
			putStrLn "connected"
			catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
			let validateQuery = "doc(\"services\")/services/service[@name=\""++name++"\" and @type=\""++typ++"\"]"
			putStrLn $ validateQuery
			catch (sednaExecute sednaCon (validateQuery)) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute")))
			queryResult <- sednaGetResult sednaCon
			let res = unpack queryResult
			let query = if res /="" then "UPDATE REPLACE $entry in doc(\"services\")/services/service[@name=\""++name++"\" and @type=\""++typ++"\"] with <service name=\""++name++"\" port=\""++port++"\" type=\""++typ++"\" updated=\"{current-date()}\" />" else "UPDATE INSERT <service name=\""++name++"\" port=\""++port++"\" type=\""++typ++"\" updated=\"{current-date()}\" /> into fn:doc(\"services\")/services"
			putStrLn $ query
			catch (sednaExecute sednaCon (query)) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute")))
			catch (sednaCommit sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCommit")
			catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")
