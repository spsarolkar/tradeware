{-# LANGUAGE OverloadedStrings #-}
import Data.Char (isPunctuation, isSpace)
import Data.Monoid (mappend)
import Data.Text (Text)
import Control.Exception (fromException)
import Control.Monad (forM_, forever)
import Control.Concurrent (MVar, newMVar, modifyMVar_, readMVar)
import Control.Monad.IO.Class (liftIO)
import Control.Monad
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Network.WebSockets as WS
import System.ZMQ3.Monadic as ZMQM
import System.ZMQ3 as ZMQ
import Data.ByteString.Char8 (pack, unpack)
import Control.Concurrent (threadDelay)
import Data.Text.Encoding
import Data.ByteString.Internal
import Control.Concurrent (forkIO)
import System.Environment

main :: IO ()
main = do
	args <- liftIO $ getArgs 
	let stockname=head args
	let port=head $ tail $ args
	let out_port=head $ tail $ tail $ args
      	liftIO $ putStrLn $ "Starting subscriber for " ++ stockname ++" "++ port
      	liftIO $ putStrLn $ "OUT PORT:: " ++ out_port
	liftIO $ putStrLn "starting main WS..."
	WS.runServer "127.0.0.1" (read out_port) $ application stockname (read port)
	--WS.runServer "0.0.0.0" (read out_port) $ application stockname (read port)

--zmqToIO :: ZMQ

application :: String->String->WS.Request -> WS.WebSockets WS.Hybi00 ()
application stockname port rq = do
	liftIO $ putStrLn "Starting websocket..."
	WS.acceptRequest rq
	sink <- WS.getSink
	WS.getVersion >>= liftIO . putStrLn . ("Client version: " ++)
	msg <- WS.receiveData
	liftIO $ putStrLn $ show $ (msg:: Text)
	
	WS.sendTextData (msg :: Text) 
	runZMQ $ do
--      		dbSocket <- ZMQM.socket Pub
--		ZMQM.bind dbSocket "tcp://*:6556"
		liftIO $ putStrLn "Staring MQ receiver..."
		subscriber <- ZMQM.socket Sub
      		ZMQM.connect subscriber ("tcp://localhost:"++port)
		ZMQM.subscribe subscriber (pack $ stockname)

      		forever $ do
          				x <- ZMQM.receive subscriber
--					ZMQM.send dbSocket [] x
          				liftIO $ putStrLn $ "########"++ (unpack $ x)
					let (z:qwords) = words $ unpack $ x
					liftIO $ putStrLn $ unwords $ qwords
					liftIO $ WS.sendSink sink $ WS.textData $ pack $ unwords $ qwords
					return ()
					--liftIO $ WS.sendSink sink $ WS.textData $ b
		return ()



{-	ZMQM.runZMQ $ do 	
		--repSocket <- ZMQM.socket ZMQM.Rep 
		repSocket <- ZMQM.socket ZMQM.Sub
		ZMQM.connect repSocket "tcp://localhost:5556" 
		ZMQM.subscribe repSocket $ pack "test"
		--ZMQM.bind repSocket "tcp://*:6555"
		--sink <- ZMQM.runZMQ $ WS.getSink
		--_ <- liftIO $ forkIO $ forever $ WS.sendSink sink $ WS.textData msg
		forever $ do
			liftIO $ putStrLn "Waiting for quote..."
			msg <- ZMQM.receive repSocket
			liftIO $ putStrLn ("Quote received:" ++ (show msg))
			--ZMQM.send repSocket [] (pack "World")
			liftIO $ WS.sendSink sink $ WS.textData msg
		return ()
-}		
--WS.sendTextData msg

	--let quote = msg2
 	--msg2 <- WS.receiveData
	--liftIO $ putStrLn $ quote
	--(WS.sendTextData $ ("test"::Text)) 
