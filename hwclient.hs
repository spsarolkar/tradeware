-- |
-- Hello World client in Haskell
-- Binds REQ socket to tcp://localhost:5555
-- Sends "Hello" to server, expects "World" back
-- 
-- Originally translated to Haskell by ERDI Gergo http://gergo.erdi.hu/

module Main where

import System.ZMQ3.Monadic
import Control.Monad (forM_, forever)
import Data.ByteString.Char8 (pack, unpack)
import Control.Concurrent (threadDelay)
import System.Random (randomRIO)

main :: IO ()
main = 
    runZMQ $ do
        liftIO $ putStrLn "Connecting to Hello World server..."  
        reqSocket <- socket Req
        connect reqSocket "tcp://localhost:4354"
	liftIO $ putStrLn $ unwords ["Sending request"]
	let update = pack $ unwords ["HUNS"]
	send reqSocket [] update
	reply <- receive reqSocket
	liftIO $ putStrLn $ unwords ["Received reply:", unpack reply]
	liftIO $ threadDelay $ 1 * 1000 * 1000

{-                zipcode <- liftIO $ randomRIO (0::Int, 100000)
                temperature <- liftIO $ randomRIO (-80::Int, 135)
                humidity <- liftIO $ randomRIO (10::Int, 60)
                let update = pack $ unwords [show zipcode, show temperature, show humidity]
                send reqSocket [] update
-}
{-        forM_ [1..10] $ \i -> do
            liftIO $ putStrLn $ unwords ["Sending request", show i]
            --send reqSocket [] (pack quote)
            send reqSocket [] $ pack $ unwords ["422011", "21", "28"]
	    liftIO $ threadDelay $ 1 * 1000 * 1000
            --reply <- receive reqSocket
            --liftIO $ putStrLn $ unwords ["Received reply:", unpack reply]    
-}


