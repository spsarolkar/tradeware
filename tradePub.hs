-- |
-- Hello World client in Haskell
-- Binds REQ socket to tcp://localhost:5555
-- Sends "Hello" to server, expects "World" back
-- 
-- Originally translated to Haskell by ERDI Gergo http://gergo.erdi.hu/

module Main where

import System.ZMQ3.Monadic
import Control.Monad (forM_, forever)
import Data.ByteString.Char8 (pack, unpack)
import Control.Concurrent (threadDelay)
import System.Random (randomRIO)

main :: IO ()
main = 
    runZMQ $ do
        liftIO $ putStrLn "Connecting to Hello World server..."  
        --reqSocket <- socket Req
        reqSocket <- socket Pub
        --connect reqSocket "tcp://localhost:6555"
        bind reqSocket "tcp://*:15877"
        --forever $ do
	forever $ do
		price <- liftIO $ randomRIO (0::Int, 1000)
		let quote = "<Mdepth><bidOffers><entry index=\"0\"><buyOrders>1</buyOrders><bestBuyQty>TESTSTSTTST</bestBuyQty><bestBuyRate>965.10</bestBuyRate><bestSellRate>967.10</bestSellRate><bestSellQty>25</bestSellQty><sellOrders>1</sellOrders></entry><entry index=\"1\"><buyOrders>1</buyOrders><bestBuyQty>2</bestBuyQty><bestBuyRate>965.05</bestBuyRate><bestSellRate>967.15</bestSellRate><bestSellQty>5</bestSellQty><sellOrders>1</sellOrders></entry><entry index=\"2\"><buyOrders>2</buyOrders><bestBuyQty>20</bestBuyQty><bestBuyRate>964.40</bestBuyRate><bestSellRate>969.00</bestSellRate><bestSellQty>50</bestSellQty><sellOrders>1</sellOrders></entry><entry index=\"3\"><buyOrders>1</buyOrders><bestBuyQty>11</bestBuyQty><bestBuyRate>962.50</bestBuyRate><bestSellRate>969.45</bestSellRate><bestSellQty>9</bestSellQty><sellOrders>1</sellOrders></entry><entry index=\"4\"><buyOrders>1</buyOrders><bestBuyQty>92</bestBuyQty><bestBuyRate>962.35</bestBuyRate><bestSellRate>969.85</bestSellRate><bestSellQty>5</bestSellQty><sellOrders>1</sellOrders></entry></bidOffers><prevIndField>965.8</prevIndField><indField>"++ (show price) ++"</indField><dqfTotBuyQty>8177</dqfTotBuyQty><dqfTotSellQty>6820</dqfTotSellQty><dqfHigh>971</dqfHigh><dqfLow>950.45</dqfLow><dqfTTQ>719</dqfTTQ><dqfClose>970.55</dqfClose><dqfTTV>690513.25</dqfTTV><dqfPerChange>-0.49</dqfPerChange><dqfWgtAvg>960.38</dqfWgtAvg><dqfOpen>971</dqfOpen><dqfLTQ>50</dqfLTQ><dqfUpperCktLt>1067.6</dqfUpperCktLt><dqfLowerCktLt>Sun Dec 08 12:26:56 IST 2013</dqfLowerCktLt><trendIndex>-72.52673</trendIndex></Mdepth>"
		send reqSocket [] $ pack $ unwords ["HUNS",quote]
		liftIO $ putStrLn $ unwords ["Sending request"]
		let update = pack $ unwords ["quote" , quote]
		send reqSocket [] update
		liftIO $ threadDelay $ 1 * 1000 * 1000

{-                zipcode <- liftIO $ randomRIO (0::Int, 100000)
                temperature <- liftIO $ randomRIO (-80::Int, 135)
                humidity <- liftIO $ randomRIO (10::Int, 60)
                let update = pack $ unwords [show zipcode, show temperature, show humidity]
                send reqSocket [] update
-}
{-        forM_ [1..10] $ \i -> do
            liftIO $ putStrLn $ unwords ["Sending request", show i]
            --send reqSocket [] (pack quote)
            send reqSocket [] $ pack $ unwords ["422011", "21", "28"]
	    liftIO $ threadDelay $ 1 * 1000 * 1000
            --reply <- receive reqSocket
            --liftIO $ putStrLn $ unwords ["Received reply:", unpack reply]    
-}


