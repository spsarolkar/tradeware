{-# LANGUAGE OverloadedStrings #-}
import Data.Char (isPunctuation, isSpace)
import Data.Monoid (mappend)
import Data.Text (Text)
import Control.Exception (fromException)
import Control.Monad (forM_, forever)
import Control.Concurrent (MVar, newMVar, modifyMVar_, readMVar)
import Control.Monad.IO.Class (liftIO)
import Control.Monad
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Network.WebSockets as WS
import System.ZMQ3.Monadic as ZMQM
import System.ZMQ3 as ZMQ
import Data.ByteString.Char8 (pack, unpack)
import Control.Concurrent (threadDelay)
import Data.Text.Encoding
import Data.ByteString.Internal
import Control.Concurrent (forkIO)
import System.Environment
import System.Process (spawnProcess,readProcess)
import Data.List(isInfixOf)
import PortUtils(getFreePort)

main :: IO ()
main = do
	liftIO $ putStrLn "starting receiver..."
	[symbol,receiver_port,db_port,websocket_port] <- liftIO $ getArgs
	--receiverPort <- getFreePort
	--websocketPort <- getFreePort

	--let cmd="lsof"
        --out <- readProcess cmd ["-i","-n","-P"] []
        --putStrLn $ show $ filter (isInfixOf "37771") $ lines out
	runZMQ $ do
		liftIO $ putStrLn $ "binding db socket "++db_port
		dbSocket <- ZMQM.socket Pub 
		ZMQM.bind dbSocket $ "tcp://*:"++db_port
		
		liftIO $ putStrLn $ "binding websocket_port socket "++websocket_port
		webSocket <- ZMQM.socket Pub
		ZMQM.bind webSocket $ "tcp://*:" ++ websocket_port
		
		liftIO $ putStrLn $ "receiver port "++receiver_port
		subscriber <- ZMQM.socket ZMQM.Sub 
		liftIO $ putStrLn $ "Starting receiver at port "++receiver_port
		ZMQM.connect subscriber $ "tcp://localhost:" ++ receiver_port
		ZMQM.subscribe subscriber (pack symbol)
		--ZMQM.bind subscriber $"tcp://*:"++receiver_port
		--ZMQM.subscribe subscriber (pack "quote")
		forever $ do
					x <- ZMQM.receive subscriber
					liftIO $ putStrLn $ unpack $ x
					let (z:qwords) = words $ unpack $ x
					ZMQM.send dbSocket [] $ pack $ unwords ("quote":qwords)
					ZMQM.send webSocket [] $ pack $ unwords (symbol:qwords)
					liftIO $ putStrLn $ unwords $ qwords
					--ZMQM.send subscriber [] $ pack $ unwords$ filter (isInfixOf "37771") $ lines out
					return ()
					--liftIO $ WS.sendSink sink $ WS.textData $ b
		return ()
