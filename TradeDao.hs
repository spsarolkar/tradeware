module TradeDao
where

import Database.SednaTypes
import Database.SednaBindings
import Prelude hiding (catch)
import Control.Exception
import Database.SednaBindings
import Database.Internal.SednaConnectionAttributes
import Database.SednaExceptions

import Data.Char (isPunctuation, isSpace)
import Data.Monoid (mappend)
import Data.Text (Text)
import Control.Exception (fromException)
import Control.Monad (forM_, forever)
import Control.Concurrent (MVar, newMVar, modifyMVar_, readMVar)
import Control.Monad.IO.Class (liftIO)
import Control.Monad
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Network.WebSockets as WS
import System.ZMQ3.Monadic as ZMQM
import System.ZMQ3 as ZMQ
import Data.ByteString.Char8 (pack, unpack)
import Control.Concurrent (threadDelay)
import Data.Text.Encoding
import Data.ByteString.Internal
import Control.Concurrent (forkIO)



dbconnect = do
        let url="localhost"
        let dbname="tradeware"
        let login="SYSTEM"
        let password="MANAGER"
        sednaConnect url dbname login password


start =	runZMQ $ do
                subscriber <- ZMQM.socket Sub
                ZMQM.connect subscriber "tcp://localhost:6556"
                ZMQM.subscribe subscriber (pack "quote")

                forever $ do
                                x <- ZMQM.receive subscriber
                                liftIO $ putStrLn $ unpack $ x
                                let (z:qwords) = words $ unpack $ x
                                liftIO $ putStrLn $ unwords $ qwords
                                liftIO $ push $ unwords $ qwords
				--liftIO $ WS.sendSink sink $ WS.textData $ pack $ unwords $ qwords
                                return ()

push :: String -> IO Bool
push xml = do
             sednaCon <- dbconnect
             putStrLn "connected"
             catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
             putStrLn "inserting new"
             let insertQuery = "UPDATE insert "++xml++" into doc(\"tradeware\")/tradedata"
             putStrLn $ insertQuery
	     catch (sednaExecute sednaCon (insertQuery)) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute")))
             catch (sednaCommit sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCommit")
             catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")
             putStrLn "disconnected"
             putStrLn "done"
             return True

