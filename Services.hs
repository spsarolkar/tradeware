module Main 
where

import System.Process (spawnProcess,readProcess,readProcessWithExitCode)
import Control.Monad.IO.Class (liftIO)
import System.ZMQ3.Monadic as ZMQM
import System.ZMQ3 as ZMQ
import Data.List.Split
import Control.Monad (forM_, forever)
import Data.ByteString.Char8 (pack, unpack)
import PortUtils(getFreePort)
import Data.Text (Text)
import ServicesDao
import Data.List (isInfixOf)

main = do
	putStrLn "Starting Services"
	let db_port=6556
	runZMQ $ do
		subscriber <- ZMQM.socket Rep 
		--ZMQM.connect subscriber "tcp://localhost:4354"
		--ZMQM.subscribe subscriber (pack "service")
		ZMQM.bind subscriber "tcp://*:4354"
		liftIO $ putStrLn "Started listening on port 4354"
		liftIO $ isExisting $ "tradeDao"
		liftIO $ spawnProcess "/usr/src/repo/tradeware/TradeDb" []
		--liftIO $ start
		forever $ do
			x <- ZMQM.receive subscriber
			liftIO $ putStrLn $ unpack $ x
			let (z:qwords) = words $ unpack $ x
			liftIO $ putStrLn $ unwords $ qwords
			--let [symbol,port] = splitOn "," $ unwords qwords
			let symbol=unpack x 
			liftIO $ putStrLn $ "Starting Services for "++unpack x
			receiver_port <- liftIO $ getFreePort
			--db_port <- liftIO $ getFreePort
			stream_port <- liftIO $ getFreePort
			streamOut_port <- liftIO $ getFreePort
			liftIO $ putStrLn $ "Ports Obtained:"++(show receiver_port) ++ " db "++(show db_port) ++" stream "++(show stream_port)++" streamOut_port "++(show streamOut_port)
			liftIO $ isExisting $ "tradeStream "++symbol
			liftIO $ isExisting $ "receiver "++symbol
			liftIO $ isExisting $ "db "++symbol
			liftIO $ spawnProcess "/usr/src/repo/tradeware/tradeStream" [symbol,show $ stream_port,show $ streamOut_port] 
			liftIO $ spawnProcess "/usr/src/repo/tradeware/receiver" [symbol,show $ receiver_port,show $ db_port,show $ stream_port] 
			liftIO $ updateService symbol (show receiver_port) "receiver"
			liftIO $ updateService symbol (show db_port) "db"
			liftIO $ updateService symbol (show stream_port) "stream_in"
			liftIO $ updateService symbol (show streamOut_port) "stream"
			ZMQM.send subscriber [] (pack $ show $ receiver_port)
			return ()


--killExistingProcesses symbol = do
--				if isExisting $ "receiver\\ "++symbol then 
				--r <- createProcess (proc "ps -ef | grep receiver\ "++symbol [])
--				putStrLn $ "result:"++out

isExisting proc_name = do
			liftIO $ putStrLn $ "checking if "++ proc_name ++" is running"
			let name = head $ words proc_name
			let symbol = head $ tail $ words proc_name
			--out <-readProcess ("ps") ["-C","tradeStream","-o","pid,args","--no-headers"] []
			(_,out,err) <- readProcessWithExitCode "ps" ["-C",name,"-o","pid,args","--no-headers"] []
			let res=filter (isInfixOf proc_name) (lines out)
			let pids = map (head.words) res
			if (length pids)>0 then do
						liftIO $ putStrLn $ "killing older processed of :"++proc_name++",pids="++ (show pids)
						out <-readProcess ("kill") pids []
						putStrLn $ out 
						liftIO $ isExisting proc_name else do liftIO $ putStrLn $ "No process identified for "++proc_name
			--let n = length $ (filter (\x -> not $ isInfixOf "grep" x) $ lines out)
			--let n = length pids
			return ()


--killExisting proc_name = do
--			liftIO $ putStrLn $ "killing process- "++ proc_name 
--			out <-readProcess ("killall "++proc_name) [] []
--			let n = length $ (filter (\x -> not $ isInfixOf "grep" x) $ lines out)
--			return $ n > 0
